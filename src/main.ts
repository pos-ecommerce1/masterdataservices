import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppModule } from './app.module';

import helmet from 'helmet';
import { envObjStart } from './config/envsub.config';
async function bootstrap() {
  await envObjStart();
  const app = await NestFactory.create(AppModule);
  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.GRPC,
    options: {
      package: 'masterdata',
      protoPath: [
        join(__dirname, 'role/role.proto'),
        join(__dirname, 'hero/hero.proto'),
      ],
    },
  });
  app.use(helmet());
  app.startAllMicroservices();
  // console.log(
  //   `🚀 User service running on port ${configService.get('http.port')}`,
  // );
}
bootstrap();
